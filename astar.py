"""A-Star search for solving graph-based problems"""

import abc
import heapq
from typing import Hashable, Iterable, MutableSequence, NamedTuple, Optional, Sequence, Tuple


class NodePath(NamedTuple):
    """A path of nodes with the total distance"""
    path: Sequence['Node']
    distance: int


class WeightedLink(NamedTuple):
    """A link to a node with an associated weight"""
    weight: int
    node: 'Node'


class LIFOPriorityQueue:
    """A queue of Nodes with associated priorities, tie-breaks are LIFO

    This datastructure uses Python's efficient heapq algorithm, combined
    with some additional metadata structures to support the following:
    - Fast retrieval of highest priority item
    - Tie-breaking in a Last-In-First-Out (LIFO) fashion
    LIFO-based action implements a depth-first search
    """
    bin_size = 100

    def __init__(self, items: Iterable[WeightedLink]):
        self._heap: list[Tuple[int, WeightedLink]] = []
        # Keep track of used heap positions for tie-braking
        self._positions: set[int] = set()
        for item in items:
            self.push(item)

    def push(self, item: WeightedLink):
        """Push an item onto the queue"""
        # Position on the heat is multiplied by bin size for tie-braking
        heap_position = item.weight * self.bin_size
        while heap_position in self._positions:
            # Last-In-First-Out for ties in the heap
            heap_position -= 1
        heap_item = (heap_position, item)
        heapq.heappush(self._heap, heap_item)
        self._positions.add(heap_position)

    def pop(self) -> WeightedLink:
        """Return the highest priority item from the queue"""
        heap_position, item = heapq.heappop(self._heap)
        self._positions.remove(heap_position)
        return item

    def __bool__(self):
        return bool(self._heap)


class Node(metaclass=abc.ABCMeta):
    """Abstract class representing a node in a graph

    A node in a graph must have certain properties:
    - Neighbours in the graph, with associated weights
    - A cost heuristic which compares a goal node, and returns a numeric result

    This class implements this interface, and assumes concrete
    versions will include metadata as necessary
    """
    def __init__(self, links: Iterable[WeightedLink]):
        self._links = list(links)
        # Stores the weight of the path inclusive the hop to here
        self._previous: Optional[WeightedLink] = None

    @abc.abstractmethod
    def cost(self, goal: 'Node') -> int:
        """A* search heuristic"""
        pass

    @property
    def previous(self) -> Optional[WeightedLink]:
        """Provides access to the previous node in a sequence"""
        return self._previous

    @previous.setter
    def previous(self, new: WeightedLink) -> None:
        """Updates the previous node in the sequence, respecting the shortest path"""
        if (self._previous is None or
                new.weight < self._previous.weight):
            self._previous = new

    @property
    def links(self) -> list[WeightedLink]:
        return self._links

    def add_link(self, link: WeightedLink) -> None:
        """Adds bidirectional link to other node"""
        if link not in self._links:
            self._links.append(link)
            return_link = WeightedLink(
                weight=link.weight,
                node=self
            )
            link.node.add_link(return_link)


def reconstruct_path(node: Node) -> NodePath:
    """Traces a path back from the endpoint"""
    if node.previous is None:
        raise Exception('Cannot reconstruct path from isolated node')
    distance = node.previous.weight
    rev_path = [node]
    current = node
    while current.previous is not None and current.previous.node != current:
        rev_path.append(current)
        current = current.previous.node
    rev_path.append(current)
    return NodePath(path=rev_path[::-1], distance=distance)


def solve(origin: Node, destination: Node) -> Optional[NodePath]:
    """Given an origin and destination node, computes the shortest path"""
    # Create a heap with just the origin to start
    queue = LIFOPriorityQueue([WeightedLink(weight=0, node=origin)])

    # Ensure the origin's previous is itself, our exit condition
    origin.previous = WeightedLink(weight=0, node=origin)
    
    # Hash table of traversed nodes lets us skip higher cost repeats
    traversed: dict[Node, int] = {}
    while queue:
        # Start with the option of smallest cost
        current = queue.pop().node
        for weight, node in current.links:
            prev_weight = current.previous.weight if current.previous is not None else 0
            node.previous = WeightedLink(
                node=current,
                weight=prev_weight + weight
            )
            if node == destination:
                return reconstruct_path(node), traversed
            cost = node.cost(destination) + node.previous.weight
            
            # Avoid re-adding an already traversed node unless we have a shorter path
            if (old_cost := traversed.get(node, None)) is not None:
                if old_cost <= cost:
                    continue
            traversed[node] = cost

            queue.push(WeightedLink(cost, node))
    return None, traversed
