import astar
from typing import List

import pytest


class SampleNode(astar.Node):
    def __init__(self, links: List[astar.Node], value: int):
        super().__init__(links)
        self.value = value

    def cost(self, goal: astar.Node) -> int:
        return abs(goal.value - self.value)


def linear_graph(num_nodes):
    nodes = []
    for i in range(num_nodes):
        node = SampleNode(links=[], value=i)
        nodes.append(node)
        if i > 0:
            link = astar.WeightedLink(weight=1, node=nodes[i-1])
            node.add_link(link)
    return nodes


@pytest.mark.parametrize(['length'], [(i,) for i in range(2, 7)])
def test_linear(length):
    graph = linear_graph(length)
    solution, _ = astar.solve(origin=graph[0], destination=graph[-1])
    assert solution is not None
    assert solution.distance == length-1
