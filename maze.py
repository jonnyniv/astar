"""Maze solver using the astar library"""

import argparse
import enum
import itertools
import math
import pathlib
from typing import List, Mapping, NamedTuple, Text


import astar


class MazeSymbol(enum.Enum):
    WALL = enum.auto()
    PATH = enum.auto()
    ORIGIN = enum.auto()
    DESTINATION = enum.auto()


CHARSET = {
    '#': MazeSymbol.WALL,
    ' ': MazeSymbol.PATH,
    's': MazeSymbol.ORIGIN,
    'e': MazeSymbol.DESTINATION,
}


class Coordinate(NamedTuple):
    x: int
    y: int


class MazeNode(astar.Node):
    def __init__(self, links: List[astar.WeightedLink], coords: Coordinate):
        super().__init__(links)
        self.coords = coords

    def manhattan_distance(self, goal: 'MazeNode') -> int:
        """Computes the distance to goal (using Manhattan distance)"""
        return 10*(abs(goal.coords.x - self.coords.x) +
                abs(goal.coords.y - self.coords.y))

    def euclidean(self, goal: 'MazeNode') -> int:
        """Computes the distance to goal (using Manhattan distance)"""
        return math.floor(100*math.sqrt(abs(goal.coords.x - self.coords.x)**2 +
                abs(goal.coords.y - self.coords.y)**2))

    def cost(self, goal):
        return self.manhattan_distance(goal)

    def __repr__(self) -> str:
        return f'MazeNode(coords={self.coords!r})'


class MazeError(Exception):
    pass


class Maze(NamedTuple):
    origin: MazeNode
    destination: MazeNode
    width: int
    height: int


def find_neighbours(
        maze: Mapping[Coordinate, MazeNode],
        pos: Coordinate
        ) -> List[MazeNode]:
    """Checks the rendered maze and returns nodes which are neighbours

    Only needs to check neighbouring nodes in the negative x, y directions,
    as we construct the maze in this direction
    """
    neighbours = []
    for offset in [(-1, 0), (0, -1)]:
        new_x = pos.x + offset[0]
        new_y = pos.y + offset[1]
        new_pos = Coordinate(x=new_x, y=new_y)
        if neighbour := maze.get(new_pos, None):
            neighbours.append(neighbour)
    return neighbours


def read_maze(maze_text: Text, charset=CHARSET) -> Maze:
    """Converts an ASCII maze into a weighted Graph"""
    # Dict with coordinate tuples as arrays allows for random access
    full_maze: dict[Coordinate, MazeNode] = {}
    row = 0
    col = 0
    max_row = 0
    max_col = 0
    origin = None
    destination = None

    for char in maze_text:
        if char == '\n':
            row += 1
            col = 0
            continue
        # Update width and height
        max_row = max(row, max_row)
        max_col = max(col, max_col)

        # Convert character to maze symbol and ignore all walls
        symbol = charset[char]
        if symbol != MazeSymbol.WALL:
            # Any non-wall symbol is a node
            pos = Coordinate(x=col, y=row)
            node = MazeNode([], pos)
            full_maze[pos] = node
            # Find neighbouring nodes and link them up
            neighbours = find_neighbours(full_maze, pos)
            for neighbour in neighbours:
                node.add_link(astar.WeightedLink(weight=1, node=neighbour))

            # Special handling for origin and destination nodes
            if symbol == MazeSymbol.ORIGIN:
                if origin is not None:
                    raise MazeError('Maze contains multiple origins')
                origin = node

            elif symbol == MazeSymbol.DESTINATION:
                if destination is not None:
                    raise MazeError('Maze contains multiple destinations')
                destination = node
        col += 1

    if origin is None or destination is None:
        raise MazeError('Maze missing origin or destination symbol')

    return Maze(origin=origin, destination=destination, width=max_col+1, height=max_row+1)


def draw_maze(maze_text: Text, maze: Maze, maze_path: astar.NodePath, traversed = {}) -> str:
    """Draw the solution onto the maze, showing the path and traversed nodes"""
    maze_list = list(maze_text)
    for node in traversed.keys():
        position = node.coords.x + (maze.width+1)*node.coords.y
        maze_list[position] = '^'


    for node in maze_path.path:
        position = node.coords.x + (maze.width+1)*node.coords.y
        maze_list[position] = 'o'
    return ''.join(maze_list)


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('maze', type=pathlib.Path)
    return parser.parse_args()


def main():
    args = parse_args()
    with args.maze.open() as maze_file:
        maze_text = maze_file.read()
    maze = read_maze(maze_text)

    solved_path, traversed = astar.solve(maze.origin, maze.destination)
    print(draw_maze(maze_text, maze, solved_path, traversed=traversed))


if __name__ == '__main__':
    main()
